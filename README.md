# Matrix-Notifications-Spaces-Scheduler
This little python script can be used to automatically change for a Matrix account the notification profile of all rooms within a space
- The idea is to be able to group rooms into private spaces and enable a notification profile for the complete space
- This is useful if you have a private and a working space, and you want to only enable notifications for the working space during working hours
- Use an additional cron job or other schedulers to call this scrip when your work starts and when your work ends

## Getting started
0. Copy the `temp.env` to `.env` and put in your server address and the authentication token for your matrix account
1. Call the scrip with a space name (-s space_name) and a profile name (-p profile_name) (sound: enable sound notifications, key: only notifications for keywords in messages, silent: no notifications)
    - Or apply the settings to a single room (-r room_name)
    - You can also specify a room (-r room_name) in a space (-s space_name)
2. Create a cron job and call the script for your spaces with the desired notification profile
