#!/usr/bin/env python3


# General
import os
import time

from dotenv import load_dotenv
import argparse

# Specific
from urllib.parse import quote
import requests
from enum import Enum


# Global variables
load_dotenv()
TOKEN = os.getenv('TOKEN')
SERVER = os.getenv('SERVER')
token = "?access_token=" + quote(TOKEN)


# Enum
class Profile(Enum):
	sound = 1
	key = 2
	silent = 3


# Other

def json_find(json_object, target_key, res):
	if type(json_object) is dict and json_object:
		for key in json_object:
			if key == target_key:
				res.append(json_object[key])
			json_find(json_object[key], target_key, res)

	elif type(json_object) is list and json_object:
		for item in json_object:
			json_find(item, target_key, res)
	return res


# Base layer

def mat_error(res):
	print("\n")
	print("Error occurred!")
	print("Status Code: " + res.status_code)
	print("Json:")
	print(res.json)


def mat_retry(res):
	time_retry = []
	json_find(res.json(), "retry_after_ms", time_retry)
	# Default time if no time specified
	time_wait = 1
	if time_retry:
		# Convert from ms to seconds
		time_wait = time_retry[0]/1000
	print("Rate limited...wait " + str(time_wait) + "s and continue...")
	# Add some buffer
	time.sleep(time_wait + 0.5)


def mat_get(url, retake=False):
	url_fix = SERVER + url + token
	res = requests.get(url_fix)
	if not retake and res.status_code == 429:
		mat_retry(res)
		return mat_get(url, retake=True)
	if res.status_code != 200:
		mat_error(res)
	return res.json()


def mat_put(url, data, retake=False):
	url_fix = SERVER + url + token
	res = requests.put(url_fix, data=data, headers={"Content-Type": "application/json"})
	if not retake and res.status_code == 429:
		mat_retry(res)
		return mat_put(url, data, retake=True)
	if res.status_code != 200:
		mat_error(res)
	return res.json()


def mat_delete(url, retake=False):
	url_fix = SERVER + url + token
	res = requests.delete(url_fix)
	if not retake and res.status_code == 429:
		mat_retry(res)
		return mat_delete(url, retake=True)
	if res.status_code != 200 and res.status_code != 404:
		mat_error(res)
	return res.json()


# Middle layer

def mat_rule_room_sound(id_val):

	return mat_put("/_matrix/client/v3/pushrules/global/room/" + quote(id_val), '{"actions" : ["notify", {"set_tweak" : "sound", "value" : "default"}]}')


def mat_rule_room_silent(id_val):

	return mat_put("/_matrix/client/v3/pushrules/global/room/" + quote(id_val), '{"actions" : ["dont_notify"]}')


def mat_rule_room_delete(id_val):

	return mat_delete("/_matrix/client/v3/pushrules/global/room/" + quote(id_val))


def mat_rule_override_silent(id_val):

	return mat_put("/_matrix/client/v3/pushrules/global/override/" + quote(id_val), '{"conditions": [{"kind": "event_match", "key": "room_id", "pattern": "' + id_val + '"}], "actions": ["dont_notify"]}')


def mat_rule_override_delete(id_val):

	return mat_delete("/_matrix/client/v3/pushrules/global/override/" + quote(id_val))


def mat_room_all():
	return mat_get("/_matrix/client/v3/joined_rooms")


def mat_room_info(id_val):
	return mat_get("/_matrix/client/v1/rooms/" + quote(id_val) + "/hierarchy")


def mat_rule_all():
	return mat_get("/_matrix/client/v3/pushrules/")


# High Layer

def mat_rule_sound(id_val):
	res1 = mat_rule_room_sound(id_val)
	res2 = mat_rule_override_delete(id_val)
	return [res1, res2]


def mat_rule_key(id_val):
	res1 = mat_rule_room_silent(id_val)
	res2 = mat_rule_override_delete(id_val)
	return [res1, res2]


def mat_rule_silent(id_val):
	res1 = mat_rule_room_delete(id_val)
	res2 = mat_rule_override_silent(id_val)
	return [res1, res2]


def mat_profile(profile, id_val):
	if profile == Profile.sound:
		mat_rule_sound(id_val)
	elif profile == Profile.key:
		mat_rule_key(id_val)
	elif profile == Profile.silent:
		mat_rule_silent(id_val)
	else:
		raise ValueError("Error internal: Profile not found")


# Functions

def list_rooms():
	# ---
	# List all rooms
	# ---
	rooms = mat_room_all()
	# Get list, present in fist dict entry
	rooms = rooms[next(iter(rooms))]
	return rooms


def find_space(space_name, room_list):
	# ---
	# Find the desired space in room list
	# ---
	space_id = None
	for room in room_list:
		if space_id is not None:
			break

		# Get room info
		room_info = mat_room_info(room)

		# Get all room types within result
		room_types = []
		room_types = json_find(room_info, "room_type", room_types)
		if room_types:
			for room_type in room_types:
				# Check if there is a space included
				if room_type == 'm.space':
					# Get name of this space
					space_names = []
					space_names = json_find(room_info, "name", space_names)
					if space_names and space_names[0] == space_name:
						space_ids = []
						space_ids = json_find(room_info, "room_id", space_ids)
						if space_ids:
							space_id = space_ids[0]
							break

	return space_id


def find_room(room_name, room_list):
	# ---
	# Find the desired room in room list
	# ---
	room_id = None
	for room in room_list:
		# Get room info
		room_info = mat_room_info(room)

		# Get all room types within result
		room_names = []
		room_names = json_find(room_info, "name", room_names)
		if room_names and room_names[0] == room_name:
			room_ids = []
			room_ids = json_find(room_info, "room_id", room_ids)
			if room_ids:
				room_id = room_ids[0]
				break

	return room_id


def find_rooms_in_space(space_id):
	# Get space hierarchy
	space_info = mat_room_info(space_id)
	room_ids = []
	room_ids = json_find(space_info, "room_id", room_ids)[1:]
	return room_ids


# Main
def main():
	# Get arguments
	help_space = "Name of the space of which the notifications should be changed"
	help_room = "Name of the room of which the notifications should be changed"
	help_profile = "Notification profile that should be applied for the (space) or (room) or (room in space). Values are (sound : display sound, key: only display sound if keywords in message, silent: silents all)"

	parser = argparse.ArgumentParser()
	parser.add_argument('-p', '--profile', help=help_profile, type=str)
	parser.add_argument('-s', '--space', help=help_space, type=str)
	parser.add_argument('-r', '--room', help=help_room, type=str)

	args = parser.parse_args()
	profile = args.profile
	space_name = args.space
	room_name = args.room

	# Check input

	if profile == "sound":
		profile = Profile.sound
	elif profile == "key":
		profile = Profile.key
	elif profile == "silent":
		profile = Profile.silent
	else:
		raise ValueError("Profile not valid!")

	if space_name and room_name:
		# Get room list
		room_list = list_rooms()
		# Find space
		space_id = find_space(space_name, room_list)
		# Check if search was successful
		if space_id is None:
			raise ValueError("Space not found!")
		# Find rooms in space
		room_list = find_rooms_in_space(space_id)
		# Find room in space
		room_id = find_room(room_name, room_list)
		# Check if search was successful
		if room_id is None:
			raise ValueError("Room not found!")
		# Apply profile
		mat_profile(profile, room_id)
		print("Change: " + room_id)
	elif space_name:
		# Get room list
		room_list = list_rooms()
		# Find space
		space_id = find_space(space_name, room_list)
		# Check if search was successful
		if space_id is None:
			raise ValueError("Space not found!")
		# Search rooms in space
		room_list = find_rooms_in_space(space_id)
		for room_id in room_list:
			mat_profile(profile, room_id)
			print("Change: " + room_id)
	elif room_name:
		# Get room list
		room_list = list_rooms()
		# Find room
		room_id = find_room(room_name, room_list)
		# Check if search was successful
		if room_id is None:
			raise ValueError("Room not found!")
		# Apply profile
		mat_profile(profile, room_id)
		print("Change: " + room_id)
	else:
		raise ValueError("You have to specify at least a space or a room!")

	print("Done!")
	return 0


if __name__ == "__main__":
	main()
